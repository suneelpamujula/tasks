package com.puzzles.day1;

public class Day3_Puzzle_01 {

	public Node reverse(Node node)
	{
		if(node==null || node.next==null)
		{
			return node;
		}
		Node temp = reverse(node.next);
		node.next.next = node;
		node.next = null;
		return temp;
	}
	public Node getNewNode(int key)
	{
		Node n = new Node();
		n.next = null;
		n.data = key;
		return n;
	}
	public Node insert(int key, Node node)
	{
		if(node==null)
		{
			return getNewNode(key);
		}
		else
		{
			node.next = insert(key, node.next);
		}
		return node;
	}
	public void printList(Node node)
	{
		if(node==null)
		{
			return ;
		}
		System.out.print(node.data+" ");
		printList(node.next);
	}
	public static void main(String[] args) {

		Node head = null;
		Day3_Puzzle_01 p = new Day3_Puzzle_01();
		head = p.insert(42, head);
		head = p.insert(23, head);
		head = p.insert(57, head);
		head = p.insert(18, head);
		head = p.insert(98, head);
		head = p.insert(76, head);
		head = p.insert(58, head);
		
		p.printList(head);
		System.out.println();
		
		Node reverseList = p.reverse(head);
		p.printList(reverseList);
		
	}

}
