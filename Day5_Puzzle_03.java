package com.puzzles.day1;

import java.util.HashMap;
import java.util.Map;

public class Day5_Puzzle_03 {

	public boolean chech(String s1, String s2)
	{
		if(s1.length()!=s2.length())
		{
			return false;
		}
		Map<Character, Character> s1map = new HashMap<>();
		Map<Character, Character> s2map = new HashMap<>();
		for(int i=0; i<s1.length(); i++)
		{
			char s1Char = s1.charAt(i);
			char s2Char = s2.charAt(i);
			if(s1map.containsKey(s1Char))
			{
				if(s1map.get(s1Char)!=s2Char)
				{
					return false;
				}
			}
			if(s2map.containsKey(s2Char))
			{
				if(s2map.get(s2Char)!=s1Char)
				{
					return false;
				}
			}
			s1map.put(s1Char, s2Char);
			s2map.put(s2Char, s1Char);
		}
		return true;
	}
	public static void main(String[] args) {

		Day5_Puzzle_03 p = new Day5_Puzzle_03();
		System.out.println(p.chech("abc", "bcd"));
		System.out.println(p.chech("egg", "add"));
		System.out.println(p.chech("foo", "bar"));
		System.out.println(p.chech("paper", "title"));
	}

}
