package com.puzzles.day1;

import java.util.Scanner;

public class Stack_01 {

	int size = 5;
	int top = -1;
	int stack[] = new int[size];
	int max = stack[0];
	int removedValue;
	Scanner sc = new Scanner(System.in);
	
	public void push()
	{
		if(top == size-1)
		{
			System.out.println("Stack is full");
		}
		else
		{
			System.out.println("Enter value");
			int value = sc.nextInt();
			top++;
			stack[top]=value;
			System.out.println("value added successfully");
		}
	}
	public Object pop()
	{
		if(top==-1)
		{
			System.out.println("Stack is empty");
			return null;
		}
		else
		{
			removedValue = stack[top];
			top--;
		}
		return removedValue;
	}
	public void peek()
	{
		if(top==-1)
		{
			System.out.println("Stack is empty");
		}
		else
		{
			System.out.println("Top Value is "+stack[top]);
		}
	}
	public void display()
	{
		System.out.println("Stack : ");
		if(top==-1)
		{
			System.out.println("Stack is empty");
		}
		else
		{
			for(int i=stack.length-1; i>=0; i--)
			{
				System.out.println(stack[i]);
			}
		}
	}
	public Object max()
	{
		if(top==-1)
		{
			System.out.println("Stack is empty");
			return null;
		}
		else
		{
			for(int i=1; i<stack.length; i++)
			{
				if(stack[i]>max)
				{
					max = stack[i];
				}
			}
		}
		return max;
	}
	
	public void showMenu()
	{
		int option;
		do {
			System.out.println("1.push    2.pop   3.peek   4.display   5.max    6.exit");
			option = sc.nextInt();
			switch(option)
			{
			case 1: push(); break;
			case 2: 
				Object returnVlue = pop();
				System.out.println("Removed value : "+returnVlue);
				break;
			case 3: peek(); break;
			case 4: display(); break;
			case 5: 
				Object maxValue = max();
				System.out.println("Maximum value is : "+maxValue);
				break;
			case 6: break;			
			default:System.out.println("Invalid option. Please enter again");
			}
		}while(option!=6);
	}
	
	public static void main(String[] args) {

	Stack_01 s = new Stack_01();
	s.showMenu();
	}

}
