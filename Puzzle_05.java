package com.puzzles.day1;

public class Puzzle_05 {

	int size = 5;
	int top = -1;
	int stack[] = new int[size];
	int max = stack[0];
	int removedValue;
	public void push(int value)
	{
		if(top==size-1)
		{
			System.out.println("Stack is full");
		}
		else
		{
			top++;
			stack[top] = value;
			System.out.println("Value added "+value);
		}
	}
	public Object pop()
	{
		if(top==-1)
		{
			return null;
		}
		else
		{
			removedValue = stack[top];
			top--;
		}
		return removedValue;
	}
	public Object max()
	{
		if(top==-1)
		{
			return null;
		}
		else
		{
			for(int i=1; i<=stack.length-1; i++)
			{
				if(stack[i]>max)
				{
					max = stack[i];
				}
			}
		}
		return max;
	}
	public void disply()
	{
		System.out.println("Stack : ");
		if(top==-1)
		{
			System.out.println("Stack is empty");
		}
		else
		{
			for(int i=stack.length-1; i>=0; i--)
			{
				System.out.println(stack[i]);
			}
		}
	}
	public static void main(String[] args) {

		Puzzle_05 p = new Puzzle_05();
		p.push(23);
		p.push(94);
		p.push(42);
		p.push(17);
		p.push(68);
		p.disply();
		Object removedValue = p.pop();
		System.out.println("Removed value : "+removedValue);
		Object maximumValue = p.max();
		System.out.println("Maximum value is : "+maximumValue);
	}
}
