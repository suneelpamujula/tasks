package com.puzzles.day1;

import java.util.Arrays;

public class Puzzle_08 {

	public int largestProduct(int arr[])
	{
		int size = arr.length;
		Arrays.sort(arr);
		int product1 = arr[size-1]*arr[size-2]*arr[size-3];
		int product2 = arr[0]*arr[1]*arr[size-1];
		return Math.max(product1, product2);
	}
	public static void main(String[] args) {
		int arr[] = {-10,-10,5,2};
		Puzzle_08 p = new Puzzle_08();
		int largestProduct = p.largestProduct(arr);
		System.out.println("The largest product is : "+largestProduct);
	}

}
