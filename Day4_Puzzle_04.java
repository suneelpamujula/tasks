package com.puzzles.day1;

import java.util.ArrayList;
import java.util.List;

public class Day4_Puzzle_04 {

	List<String> l = new ArrayList<String>();
	public String swapElements(String str, int left, int right) 
	{
		char temp;
		char arr[] = str.toCharArray();
		temp = arr[left];
		arr[left] = arr[right];
		arr[right] = temp;
		return String.valueOf(arr);
	}

	public void printPermutations(String str, int left, int right)
	{
		if (left == right)
		{
			//System.out.println(str);
			l.add(str);
		} 
		else
		{
			for (int i = left; i <= right; i++) 
			{
				str = swapElements(str, left, i);
				printPermutations(str, left + 1, right);
				str = swapElements(str, i, left);
			}
		}
	}

	public boolean chekPalindrome(String str)
	{
		char[] y = str.toCharArray();
		int size = y.length;
		int a[] = new int[size];
		int i = 0;
		while (i != size)
		{
			a[size - i - 1] = y[i];
			i++;
		}
		int j = 0;
		while (j != size)
		{
			if (a[j] != y[j]) {
				return false;
			}
			j++;
		}
		return true;
	}

	public boolean checkPalindromePermutations(String str, int left, int right)
	{
		printPermutations(str,left,right);
		for (int i = 0; i < l.size(); i++)
		{
			if (chekPalindrome(l.get(i))) 
			{
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) {

		Day4_Puzzle_04 p = new Day4_Puzzle_04();
		String s = "acbca";
		System.out.println(p.checkPalindromePermutations(s, 0, s.length()-1));
	}

}
