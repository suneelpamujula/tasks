package com.puzzles.day1;

public class Puzzle_09 {

	public int checkPerfect(int number)
	{
		int value = 0;
		if(number<=10)
		{
			value = 10-number;
		}
		else
		{
			return -1;
		}
		String result = number+""+value;
		
		return Integer.parseInt(result);
	}
	public static void main(String[] args) {

		Puzzle_09 p = new Puzzle_09();
	    System.out.println(p.checkPerfect(2));
	}

}
