package com.puzzles.day1;

public class Puzzle_10 {

	public int maximumProfit(int arr[])
	{
		int minimumPrice = arr[0];
		int profit = 0;
		for(int i=1; i<arr.length; i++)
		{
//			if(minimumPrice>arr[i])
//			{
//				minimumPrice=arr[i];
//			}
//			if(profit<arr[i]-minimumPrice)
//			{
//				profit = arr[i];
//			}
			minimumPrice = Math.min(minimumPrice, arr[i]);
			profit = Math.max(profit, arr[i]-minimumPrice);
		}
		return profit;
	}
	public static void main(String[] args) {

		int arr[] = {9,11,8,5,7,10};
		Puzzle_10 p = new Puzzle_10();
		int maximumProfit = p.maximumProfit(arr);
		System.out.println("Maxium profit : "+maximumProfit);
	}

}
