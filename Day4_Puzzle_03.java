package com.puzzles.day1;

public class Day4_Puzzle_03 {

	int minSum = 0;
	public void minSumFromRootToLeaf(Node node, int sum)
	{
		if (node == null)
		{
			return;
		}
		if (node.left == null && node.right == null)
		{
			if(sum+node.data<minSum)
			{
				minSum = sum + node.data;
			}
			return;
		}
		minSumFromRootToLeaf(node.left, sum - node.data);
		minSumFromRootToLeaf(node.right, sum - node.data);
	}

	public int minimumSum(Node node, int sum)
	{
		minSumFromRootToLeaf(node, 0);
		return minSum;
	}
//	public int minSumFromRootToLeaf(Node node) {
//		if(node == null)
//		{
//			return 0;
//		}
//		int sum = node.data;
//		
//		if(node.left==null&&node.right==null)
//		{
//			int leftSum = minPathSum(node.left);
//			int rightSum = minPathSum(node.right);
//			if(leftSum < rightSum){
//				sum = sum + leftSum;
//			}else{
//				sum = sum + rightSum;
//			}
//		}
//		
//		return sum;
//	}
	public Node getNewNode(int value)
	{
		Node n = new Node();
		n.data = value;
		n.left = null;
		n.right = null;
		return n;
	}

	public Node insert(int value, Node node) 
	{
		if (node == null) 
		{
			return getNewNode(value);
		} else 
		{
			if (value < node.data)
			{
				node.left = insert(value, node.left);
			} 
			else if (value > node.data)
			{
				node.right = insert(value, node.right);
			}
		}
		return node;
	}

	public static void main(String[] args) {

		Node root = null;
		Day4_Puzzle_03 p = new Day4_Puzzle_03();
		root = p.insert(2, root);
		root = p.insert(7, root);
		root = p.insert(5, root);
		root = p.insert(2, root);
		root = p.insert(6, root);
		root = p.insert(5, root);
		root = p.insert(11, root);
		root = p.insert(9, root);
		root = p.insert(4, root);
		
		System.out.println(p.minimumSum(root, 0));

	}

}
