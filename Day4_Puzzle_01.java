package com.puzzles.day1;

import java.util.Vector;

public class Day4_Puzzle_01 {

	Vector<Integer> v = new Vector<>();
	public void record(int timeStamp)
	{
		v.add(timeStamp);
	}
	public int range(int start, int end)
	{
		int count = 0;
		for(int i=start; i<=end; i++)
		{
			count++;
		}
		return count;
	}
	public int total()
	{
		return v.size();
	}
	public static void main(String[] args) {

		Day4_Puzzle_01 p = new Day4_Puzzle_01();
		p.record(1);
		p.record(2);
		p.record(3);
		p.record(4);
		p.record(5);
		System.out.println(p.total());
		System.out.println(p.range(1, 3));
	}

}
