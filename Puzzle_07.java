package com.puzzles.day1;

public class Puzzle_07 {

	public boolean checkEqualSubsets(int arr[])
	{
		if(arr.length==0)
		{
			return false;
		}
		int sum = 0;
		for(int i=0; i<arr.length; i++)
		{
			sum = sum + arr[i];
		}
		if(sum%2 != 0)
		{
			return false;
		}
		sum = sum/2;
		return false;
	}
	public static void main(String[] args) {

		int arr[] = {15,5,20,10,35,15,10};
		Puzzle_07 p = new Puzzle_07();
		System.out.println(p.checkEqualSubsets(arr));
	}

}
