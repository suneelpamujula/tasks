package com.puzzles.day1;

public class Day5_Puzzle_04 {
	
	public Node rotateList(Node node, int k) {
	    if(node == null || k < 0) 
	    {
	      return node;
	    }
	    int sizeOfList = size(node);
	    k = k % sizeOfList;
	    if(k == 0) 
	    {
	      return node;
	    }
	    Node t = node;
	    int i = 1;
	    while(i < sizeOfList - k)
	    {
	      t = t.next;
	      i++;
	    }
	    Node temp = t.next;
	    Node head = temp;
	    t.next = null;
	    while(temp.next != null)
	    {
	      temp = temp.next;
	    }
	    temp.next = node;
	    return head;
	  }
	public Node getNewNode(int value)
	{
		Node n = new Node();
		n.data = value;
		n.next = null;
		return n;
	}
	public Node insert(int value, Node node)
	{
		if(node==null)
		{
			return getNewNode(value);
		}
		else
		{
			node.next = insert(value, node.next);
		}
		return node;
	}
	
	public void printList(Node node)
	{
		if(node==null)
		{
			return ;
		}
		else
		{
			System.out.print(node.data+" ");
		}
		printList(node.next);
	}
	public int size(Node node)
	{
		if(node==null)
		{
			return 0;
		}
		return 1+size(node.next);
	}

	public static void main(String[] args) {

		Node root = null;
		Day5_Puzzle_04 p = new Day5_Puzzle_04();
		root = p.insert(7, root);
	    root = p.insert(7, root);
	    root = p.insert(3, root);
	    root = p.insert(5, root);
	    
	    p.printList(root);
	    System.out.println();
	    
	    root = p.rotateList(root, 2);
	    p.printList(root);
	    System.out.println();
	}

}
