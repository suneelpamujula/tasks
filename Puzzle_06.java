package com.puzzles.day1;

public class Puzzle_06 {

	public boolean checkTarget(char arr[][], String target)
	{
		for(int i=0; i<arr.length; i++)
		{
			String rowString = "";
			String columnString = "";
			for(int j=0; j<arr[i].length; j++)
			{
				rowString = rowString + arr[i][j];
				columnString = columnString + arr[j][i];
			}
			if(rowString.equals(target) || columnString.equals(target))
			{
				return true;
			}
		}
		return false;
	}
	public static void main(String[] args) {
		
		char arr[][] = {
				{'F','A','C','I'},
				{'O','B','Q','P'},
				{'A','N','O','B'},
				{'M','A','S','S'}
				};
		Puzzle_06 p = new Puzzle_06();
		System.out.println(p.checkTarget(arr, "MASS"));
	}

}
