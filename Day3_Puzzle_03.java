package com.puzzles.day1;

import java.io.FileReader;
import java.io.IOException;

public class Day3_Puzzle_03 {

	public String readUptoNthCharacter(int n) throws IOException {
		String result = "";
		FileReader fr = new FileReader("file1.txt");
		int i = fr.read();
		int count = 0;

		while (i != -1)
		{
			if (count >= n)
			{
				break;
			}
			result = result + (char) i;
			i = fr.read();
			count++;	
		}
		fr.close();
		return result;
	}

	public static void main(String[] args) throws IOException {

		Day3_Puzzle_03 p = new Day3_Puzzle_03();
		System.out.println(p.readUptoNthCharacter(14));
	}

}
