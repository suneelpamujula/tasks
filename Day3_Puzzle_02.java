package com.puzzles.day1;

public class Day3_Puzzle_02 {

	public Node getNewNode(int value)
	{
		Node n = new Node();
		n.data = value;
		n.left = null;
		n.right = null;
		return n;
	}
	public Node insert(int value, Node node)
	{
		if(node == null)
		{
			return getNewNode(value);
		}
		else
		{
			if(value<node.data)
			{
				node.left = insert(value, node.left);
			}
			else if(value>node.data)
			{
				node.right = insert(value, node.right);
			}
		}
		return node;
	}
//	public Node deepestNode(Node node)
//	{
//		
//	}
	public void printList(Node node)//Sorting order
	{
		if(node==null)
		{
			return ;
		}
		else
		{
			printList(node.left);
			System.out.print(node.data+" ");
			printList(node.right);
		}
	}
	public static void main(String[] args) {

		Node root = null;
		Day3_Puzzle_02 p = new Day3_Puzzle_02();
		root = p.insert(14, root);
		root = p.insert(5, root);
		root = p.insert(14, root);
		root = p.insert(23, root);
		root = p.insert(18, root);
		root = p.insert(2, root);
		root = p.insert(4, root);
		root = p.insert(22, root);
		root = p.insert(13, root);
		root = p.insert(17, root);
		
		p.printList(root);
	}

}
