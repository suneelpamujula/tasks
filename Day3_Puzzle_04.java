package com.puzzles.day1;

public class Day3_Puzzle_04 {

	public int islandCount(int arr[][]) {
		int r = arr.length;
		int c = arr[0].length;
		boolean islandReached[][] = new boolean[r][c];
		int count = 0;
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				if (arr[i][j] == 1 && !islandReached[i][j]) {
					islandCountHelper(arr, i, j, islandReached);
					count++;
				}
			}
		}
		return count;
	}

	public void islandCountHelper(int arr[][], int x, int y, boolean[][] islandReached) {
		islandReached[x][y] = true;

		if (check(arr, x - 1, y - 1, islandReached)) {
			islandCountHelper(arr, x - 1, y - 1, islandReached);
		}

		if (check(arr, x - 1, y, islandReached)) {
			islandCountHelper(arr, x - 1, y, islandReached);
		}

		if (check(arr, x - 1, y + 1, islandReached)) {
			islandCountHelper(arr, x - 1, y + 1, islandReached);
		}

		if (check(arr, x, y - 1, islandReached)) {
			islandCountHelper(arr, x, y - 1, islandReached);
		}

		if (check(arr, x, y + 1, islandReached)) {
			islandCountHelper(arr, x, y + 1, islandReached);
		}

		if (check(arr, x + 1, y - 1, islandReached)) {
			islandCountHelper(arr, x + 1, y - 1, islandReached);
		}

		if (check(arr, x + 1, y, islandReached)) {
			islandCountHelper(arr, x + 1, y, islandReached);
		}

		if (check(arr, x + 1, y + 1, islandReached)) {
			islandCountHelper(arr, x + 1, y + 1, islandReached);
		}

	}

	public boolean check(int arr[][], int x, int y, boolean[][] islandReached) {
		int r = arr.length;
		int c = arr[0].length;
		return x >= 0 && x < r
			&& y >= 0 && y < c
			&& arr[x][y] == 1 
			&& !islandReached[x][y];
	}

	public static void main(String[] args) {

		int arr[][] = {
				{1,0,0,0,0},
				{0,0,1,1,0},
				{0,1,1,0,0},
				{0,0,0,0,0},
				{1,1,0,0,1},
				{1,1,0,0,1}
			};

		Day3_Puzzle_04 a = new Day3_Puzzle_04();
		int noOfIslands = a.islandCount(arr);
		System.out.println("Number of Islands : "+noOfIslands);
	}
}
