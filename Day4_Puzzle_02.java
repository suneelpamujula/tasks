package com.puzzles.day1;

import java.util.ArrayList;
import java.util.List;

public class Day4_Puzzle_02 {
	List<Integer> l = new ArrayList();
	public void init(int arr[], int size)
	{
		for(int i=0; i<size; i++)
		{
			if(arr[i]!=0)
			{
				l.set(i, arr[i]);
			}
		}
	}
	public int get(int index)
	{
		return l.get(index);
	}
	public void set(int index, int value)
	{
		l.set(index, value);
	}
	public static void main(String[] args) {

		Day4_Puzzle_02 p = new Day4_Puzzle_02();
		int arr[] = {3,0,0,9,0,4,0,9,0,0,12,0,0,14,17,5};
		for(int i=0; i<arr.length; i++)
		{
			p.l.add(0);
		}
		p.init(arr, arr.length);
		System.out.println(p.l);
		System.out.println("-----------------------------------------------------");
		System.out.println(p.get(5));
		System.out.println(p.get(8));
		p.set(6, 44);
		System.out.println(p.get(6));
		
	}

}
