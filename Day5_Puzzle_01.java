package com.puzzles.day1;

import java.util.HashMap;
import java.util.Map;

public class Day5_Puzzle_01 {
	
		Map<String, Object> map = new HashMap<>();

		public Map<String, Object> flattenDictionary(Map<String, Object> m, String str)
		{
			m.forEach((key,value)->{
				if(key!="")
				{
					if(str!="")
					{
						key = str+"."+key;
					}
				}
				else
				{
					key = str;
				}
			});
			return m;
		}
		public static void main(String[] args) {

		Day5_Puzzle_01 p = new Day5_Puzzle_01();
		
		Map<String, Object> m1 = new HashMap<>();
		Map<String, Integer> m2 = new HashMap<>();
		m2.put("baz", 8);
		m1.put("a", 5);
		m1.put("bar", m2);
		p.map.put("key", 3);
		p.map.put("foo", m1);
				
		String str = "";
		System.out.println(p.flattenDictionary(p.map, str));
//		for(Map.Entry<String, Object> data : p.map.entrySet())
//		{
//			System.out.println(data.getKey()+"------"+data.getValue());
//		}
		
		
		
		
	}

}
